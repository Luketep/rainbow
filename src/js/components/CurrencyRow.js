import React from 'react'
import PropTypes from 'prop-types'

class CurrencyRow extends React.Component {
  render() {
    const { token, amount, single, total } = this.props
    return (
      <tr>
        <td>{token}</td>
        <td>{amount}</td>
        <td>$ {single} USD</td>
        <td>$ {total} USD</td>
      </tr>
    )
  }
}

CurrencyRow.propTypes = {
  token: PropTypes.string,
  amount: PropTypes.number,
  single: PropTypes.number,
  total: PropTypes.string
}

export default CurrencyRow