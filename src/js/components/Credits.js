import React from 'react'

export default () => (
    <section className="section container">
        <h1 className="title">Credits</h1>
        Developed using <a href="https://reactjs.org/" target="_blank" rel="noopener noreferrer">React</a> and <a href="https://bulma.io/" target="_blank" rel="noopener noreferrer">Burma</a><br />
        Favicon by <a href="http://whitewaterwest.com/">Kurt Gosselin</a>, hosted at <a href="http://www.iconj.com/favicon-c38s8rzw34.html" target="_blank" rel="noopener noreferrer">IconJ</a><br />
        Testing & Consulting by <a href="https://github.com/jkyberneees" target="_blank" rel="noopener noreferrer">Rolando Santamaria Maso</a><br />
        Developed by <a href="https://twitter.com/luketep" target="_blank" rel="noopener noreferrer">Philip Langbehn</a>, code is available on <a href="https://gitlab.com/luketep/rainbow" target="_blank" rel="noopener noreferrer">Github</a>
    </section>
)
